# -*- coding: utf-8 -*-
"""
Created on Sat Nov  3 21:57:27 2018

@author: zoya
"""

import vk_api


        
class User(object):
    
    api = vk_api.VkApi(token="e3d164dbe017c8198b226b3ffe5fba61e2e8cfb92cdac1495fc3eccb028aa73586b86c86acfa4d6053873")
    
    def __init__(self, _id_):
        self.friends = []
        self.id  = _id_
        
    def __str__(self):
        human = "___"+self.id+"___::\n"
        for friend in self.friends:
            human += friend.id +"\n"        
        return human
    
    def load_friends(self):
        order='hints'
        friends = self.api.method('friends.get', {'user_id': self.id, 'order': order,})
        friends = friends['items']
        for friend_id in friends:
            person = User(str(friend_id))
           # person.get_by_id(friend_id)
            self.friends.append(person)
        return self
    
    def get_friends(self):
        return self.friends
  

if __name__ == "__main__":
    human = User("2707088")
    human.load_friends()
    print(human.get_friends())
        
    