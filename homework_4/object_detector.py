# -*- coding: utf-8 -*-
"""
Редактор Spyder

Это временный скриптовый файл.
"""

import numpy as np
from scipy.ndimage import morphology
from skimage.measure import label
import matplotlib.pyplot as plt

img = np.load("ps.npy.txt")

elem = np.asarray([[1,1,0,0,1,1],
                    [1,1,0,0,1,1],
                    [1,1,1,1,1,1],
                    [1,1,1,1,1,1]
                    ])

sqere = np.asarray([[1,1,1,1,1,1],
                    [1,1,1,1,1,1],
                    [1,1,1,1,1,1],
                    [1,1,1,1,1,1]
                    ])

sqeres = morphology.binary_opening(img, sqere).astype("uint16")
sqere_result = label(sqeres)

count = len(np.unique(sqere_result)) - 1
print("Count of sqeres: ", count)

objects = img - sqeres

for i in range(0,4):
    
    result = morphology.binary_erosion(objects.copy(), elem).astype("uint16")
    result = label(result)
    count = len(np.unique(result)) - 1
    
    print("Count of Figure",i,count)
    elem = np.rot90(elem)

#1 92
#2 st type quantity:  94
#3 st type quantity:  95
#4 st type quantity:  123
#5 st type quantity:  96