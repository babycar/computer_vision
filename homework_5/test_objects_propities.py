# -*- coding: utf-8 -*-
"""
Created on Mon Nov 12 18:54:11 2018

@author: Acer
"""

import numpy as np
from objects_propities import Object2D
import unittest
import matplotlib.pyplot as plt
 
def img1():    
    LB = np.zeros((16,16))
    LB[4:,:4] = 2
    LB[12:-1, 6:9] = 3
    
    LB[3:10, 8:] = 1
    LB[[3,4,3], [8,8,9]] = 0
    LB[[8,9,9], [8,8,9]] = 0
    LB[[3,4,3], [-2,-1,-1]] = 0
    LB[[9,8,9], [-2,-1,-1]] = 0    
    return LB

class Object2DTest(unittest.TestCase):

    def test_perimetr(self):
        
         test1 = img1()
         
         fig1 = Object2D(test1,1)  
         fig2 = Object2D(test1,2)  
         fig3 = Object2D(test1,3)          
         
         self.assertEqual(fig1.perimetr(), 21.313708498984759)      
         self.assertEqual(fig2.perimetr(), 28)  
         self.assertEqual(fig3.perimetr(), 8)  

            
if __name__ == "__main__":    
    #unittest.main()
    fig = Object2D(img1(),1)  
    img = fig.drow_boundaries()
    print(fig.perimetr())


