# -*- coding: utf-8 -*-
"""
Created on Fri Nov  2 19:28:23 2018

@author: Acer
"""

import numpy as np
from skimage.measure import label
import matplotlib.pyplot as plt

def neighbors(y,x):
        return ((y, x+1), (y+1, x), (y, x-1), (y-1, x))
    
def diag_neighbors(y,x):
    return ((y+1, x+1), (y-1, x-1), (y+1, x-1), (y-1, x+1))
    
def dist(point1, point2):
    x1,y1 = point1
    x2,y2 = point2
    d = ((x1-x2)**2 + (y1-y2)**2)**0.5
    return d

class Object2D(object):
    
    def __init__(self, shape, sh_label):
        self.shape = shape     
        self.label = sh_label
        self.usable = []
    
    def __get_boundaries(self):
        pos = np.where(self.shape == self.label)
        boundaries = []
        for y, x in zip(*pos):
            for yn, xn in neighbors(y, x):
                if yn < 0 or yn >= self.shape.shape[0] :
                    boundaries.append((y, x))
                elif xn < 0 or xn >= self.shape.shape[1]:
                    boundaries.append((y, x))
                elif self.shape[yn, xn] != self.label:
                    boundaries.append((y, x))
                    break
        return boundaries
        
    def calc_area(self):
         return len(np.where(self.shape == self.label)[0])
     
    def calc_centr(self):
        X = np.where(self.shape == self.label)[0]
        Y = np.where(self.shape == self.label)[1]
        return ((np.mean(X)), (np.mean(Y)))
    
    def drow_boundaries(self):
        BB = np.zeros_like(self.shape)
        BB [np.where(self.shape == self.label)] = 1
        b = self.__get_boundaries()
       # print(b)
        for y,x in self.__get_boundaries():
            BB[y,x] = 2
        return BB      
    
    def __is_angle(self, desc):
         anglsz = np.asarray([[[0,0,0],
                 [0,1,1],
                 [0,1,0]],
    
                [[0,0,1],
                 [0,1,0],
                 [0,1,0]],
                 
                 [[1,0,0],
                  [0,1,0],
                 [0,1,0]],
                 
                 [[0,0,1],
                 [0,1,0],
                 [0,0,1]],  
                  
                 [[0,0,0],
                 [0,1,1],
                 [0,0,1]],
                  
                 [[1,1,1],
                 [0,1,0],
                 [1,0,0]],
                  
                 [[0,0,1],
                 [0,1,0],
                 [0,1,1]],  
            
                ])
         
         anglst = anglsz[::,::-1,::]
         anglse = anglsz[::,::,::-1]
         
         angls = np.vstack((anglst, anglse, anglsz))
         #angls =  np.vstack(anglsz, angls)
         
         #print(anglst)
         for angle in angls:
                for i in range(5):
                    angle_rot = np.rot90(angle, i)
                    if (desc == angle_rot).all():
                        return True
         return False
        
        
    def __find_angle(self):                
        
        BB = np.zeros_like(self.shape)
        BB [np.where(self.shape == self.label)] = 0
        bondaries = self.__get_boundaries()
        coords = np.asarray(bondaries)
        if len(bondaries)<=9:
            return bondaries
        for y,x in coords:
            BB[y,x] = 1
        buffer = np.zeros((BB.shape[0]+2,BB.shape[1]+2))
        buffer[1:-1,1:-1] = BB
        buffer_copy = buffer.copy()
        coords += 1
        angles = []
        for y,x in coords:
            desc = buffer[y-1:y+2, x-1:x+2]
            if self.__is_angle(desc):
               # print(y,x)
                #print(desc)
                buffer_copy[y,x] = 3
                if (y-1, x-1) not in angles:
                    angles.append((y-1,x-1))
                
        return angles
    
    def perimetr(self):
        angles = self.__find_angle()
        #print(angles)
        #print(angles)
        visited = [angles[0]]
        p = 0
        while len(visited) != len(angles):
            angles = sorted(angles, key = lambda point: dist(point, visited[-1]))
            for angle in angles:
                if angle not in visited:
                    visited+=[angle]
                    break
            p+= dist(visited[-2], visited[-1])
        p+= dist (visited[0], visited[-1])
        
        return p
    
    def get_perimetr(self):
        return len(self.__get_boundaries())
    
    def circularity(self):
        return (self.get_perimetr()**2)/self.calc_area()
    
    def radial_distance(self):
        r,c = self.calc_centr()
        boundaries = self.__get_boundaries()
        K = len(boundaries)
        rd = 0
        for rk, ck in boundaries:
            rd += dist((r,c), (rk,ck))
        return rd/K
    
    def std_radial(self):
        r, c = self.calc_centr()
        boundaries = self.__get_boundaries()
        rd = self.radial_distance()
        K = len(boundaries)
        sr = 0
        for rk, ck in boundaries:
            sr += (dist((r,c ), (rk,ck)) - rd)**2
        return (sr/K)**0.5
    
    def circularity_std(self):
        return (self.radial_distance())/self.std_radial()
        
        
    

    

    
    
    
    