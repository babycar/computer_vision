# -*- coding: utf-8 -*-
"""
Created on Sun Dec 16 11:52:54 2018

@author: zoya
"""
import cv2
import numpy as np
from matplotlib import pyplot as plt
from skimage.filters import threshold_mean
from PIL import Image, ImageGrab
from mss import mss
import datetime

import pyautogui           
#img = ImageGrab.grab()

template = cv2.imread('tRex.png',0)
w, h = template.shape[::-1]


def find_trex():
    
    with mss() as sct:
        monitor = {"top": 0, "left": 0, "width": 1400, "height": 600}
        img_rgb =np.array(sct.grab(monitor))
#        print(img_rgb)
    img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)      #
    res = cv2.matchTemplate(img_gray,template,cv2.TM_CCOEFF_NORMED)    #    
    threshold = 0.8
    
    try:
        loc = np.where( res >= threshold)   
        position = list(zip(*loc[::-1]))[0]    
        center = (position[1]+(h//2), position[0]+(w//2))
        
    except:
        
        return False
    return center
                        
def press():
    pyautogui.keyDown('space')
      
#img_gray = None             
def game(center):             

    global img_gray         
    global binary             
    global area
    for i in range(10000):
        with mss() as sct:
            monitor = {"top": 0, "left" : 0, "width": 1400, "height": 600}
            img_rgb =np.array(sct.grab(monitor))
        img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)   
        detect_dist = 95                                                              
        point = (center[0]+detect_dist, center[1]-20)
         
        thresh = threshold_mean(img_gray)        
        binary = (img_gray > thresh).astype(int)     
        x_start = center[0] + detect_dist              
        y_start = center[1]            
        
        coor = (x_start, y_start)
        print(center)
        area = binary[coor[1]-75:coor[1]-55, coor[0]:coor[0]+95]
             
        if 0 in area:
#            plt.imshow(area)
#            plt.show()
            press()      
            print("JUMP!")
            
    print(datetime.datetime.now())
    print("end")
    return
    
        
center = False
for i in range(100000):
    center = find_trex()
    if center:
        game(center) 
#        break
#
#if center:
#    game(center)            
    

#thresh = threshold_mean(img_gray)        
#binary = (img_gray > thresh).astype(int) 
#
#center = (473, 537)
#detect_dist = 165  
#x_start = center[0] + detect_dist
#y_start = center[1]-50
##
#coor =( 578,487 )
##area = binary[x_start - 500: x_start, y_start :y_start + 500]
##img = cv2.circle(binary, (x_start, y_start), 10, (0,0,255),2)
##
#img = cv2.circle(binary.copy(),coor, 7, (0,0,255),2)
##
#img = cv2.circle(img ,(coor[0]+50, coor[1]-50), 7, (0,0,255),2)
#img = cv2.circle(img ,(coor[0]+10, coor[1]-10), 7, (0,0,255),2)
##
#bimg = img[coor[1]-50:coor[1], coor[0]:coor[0]+50]
#plt.imshow(bimg)
#plt.show()



#plt.imshow(binary)
#plt.scatter(537, 473)
#plt.show()
#for i in range(1000):
#    press()
                             



