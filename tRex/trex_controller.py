# -*- coding: utf-8 -*-
"""
Created on Tue Jan  1 13:09:05 2019

@author: zoya
"""

import cv2
import numpy as np
from matplotlib import pyplot as plt
from skimage.filters import threshold_mean
from PIL import Image, ImageGrab
from mss import mss
import datetime
import pyautogui


tRex = cv2.imread('tRex.png',0)
width, height = tRex.shape[::-1]

#V = 200

def press():
    pyautogui.keyDown('space')
    
def get_img():
   with mss() as sct:
        monitor = {"top": 0, "left" : 0, "width": 1400, "height": 600}
        img_rgb =np.array(sct.grab(monitor))
   return img_rgb
    
def find_trex(img_rgb):
    img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)      #
    res = cv2.matchTemplate(img_gray,tRex,cv2.TM_CCOEFF_NORMED)    #    
    threshold = 0.8    
    try:
        loc = np.where( res >= threshold)   
        position = list(zip(*loc[::-1]))[0]    
        center = position[0]+(width//2), position[1]+(height//2)      
    except:        
        return False
    return center

def game(center):    
    global zone        
    V = 120         
    for i in range(10000):
        img_rgb = get_img()
        img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)                                                               
        thresh = threshold_mean(img_gray)        
        binary = (img_gray > thresh).astype(int)  
        x_start, x_end = center[0] + width//2, center[0] + width//2 + V
        y_start, y_end = center[1]- height//2 - 2, center[1] + height//2 - 15 
        control_point = not binary[center[0], center[1] + height]
        zone = binary[y_start: y_end, x_start: x_end]
        if control_point in zone:
            press()      
            print("JUMP!") 
            V+=1
    print(datetime.datetime.now())
    print("end")
    return

def test1():
    screen_example = cv2.imread('screen_example.png')
    center = find_trex(screen_example)         
    #cv2.circle(screen_example, center, 4, (0,0,255), 10, 1)
    p1 = center[0] + width//2, center[1]+ height//2 - 10
    p2 = center[0] + width//2+V, center[1]- height//2-5    
    #cv2.circle(screen_example, p1, 4, (0,0,255), 10, 1)
    #cv2.circle(screen_example, p2, 4, (0,0,255), 10, 1)
    cv2.rectangle(screen_example,p2,p1,(0,255,0),1)
    plt.imshow(screen_example)
    plt.show()
             
def test2():
     screen_example = cv2.imread('screen_example.png')
     center = find_trex(screen_example)         
     V = 200
     x_start, x_end = center[0] + width//2, center[0] + width//2 + V
     y_start, y_end = center[1]- height//2 - 5, center[1] + height//2 - 10 
     zone = screen_example[y_start: y_end, x_start: x_end]
     
     plt.imshow(zone)
     plt.show()
    
if __name__ == "__main__":
#    test2()
    
    center = False
    for i in range(100000):
        center = find_trex(get_img())
        if center:
            print("FIND!")
            game(center) 
